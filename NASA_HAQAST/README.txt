ORDER OF OPERATIONS:
***Figuring this process out was difficult, and is similarly difficult to explain. Please email rainamonaghan@gmail.com if you need help beyond what is available.***
***General Methodology on Gdrive and also located in /Docs_and_Lit/
1.	There is a fishnet grid covering the EVAA (bounds determined by project, not authoritative), of which Emily has randomly selected a select few (50)
2.	For each of these, there are subdirectories within Gdrive of Sentinel2 Imagery covering the area
3.	Bring these into Pro--run Get_Filelist.ipynb--this will get you a composite from the various bands, as well as ABAI/NBR
4. 	Bring in fishnet grid into Pro, as well--search for your grid cell by ID (where imagery is shown)
5.	Bring in template from /Data/Template/--make a copy of this. 
6.	Chronologically order imagery and NBR, and digitize fires
		- Symbolizing by RGB band combination Red, NIR, SWIR worked best
		- Digitize if:
			- Fire occurred within cell
			- Fire intersects cell boundary
			- Fire touches boundary of another digitized fire
				- Because of this condition, some fires that you digitize will end up being quite far away from the cell boundaries
				- To fulfill digitizations for this condition, you will have to digitize everything chronologically, and then check again to see
				  if you have undigitized fires that are touching your first round digitizations
				- This condition is a bit more lax--if you are simply drifting too far out, and it feels ridiculous, stop
		- Watch out for:
			- Inundation that looks like a burn scar
			- Using burn permit db to verify can become tricky, because points in the db don't always represent individual fires
		- You don't have to make an individual fire for each field--you can group these if the divisions are not too glaring
7.	Using template copy, fill out attributes - you will need the image date of the last clear image, and a post-burn date
		- For the Flint Hills, the post-burn date was named as such since their fires can span multiple days
		- For EVAA, since fires generally burn within one day, you will not need to look for images where you believe the fire is active--when you see the burn scar,
		  that's the post-burn date
		- There is a good explanation of what the date columns are in the "General Methodology" doc referenced in the subdirectory in *** #2
8.	For each group of digitized polygons that have the same pre- and post- fire date, export to its own feature layer (naming convention in "General Methodology" doc)
9.	Last step is to sjoin fire detection products to digitization based on some distance. Emily has nixed this step. Your gdb should only contain fire digitizations.
9.	Every two weeks or so, upload your gdb to Gdrive (recommended)

***For other information on postprocessing steps, look at /Docs_and_Lit/ Task 6 Update doc

OTHER NOTES:
	- /Scripts/planet_availability.ipynb: This project was supposed to consist of digitizing both S2 and Planet. This script may be of use if there is interest in 
	  downloading Planet imagery through the API (which is pretty straight forward, and considerably easier than navigating site).
	- /Scripts/select_by_location.ipynb: Used for detecting other satellite fire detections (as mentioned above, this is unnecessary now).
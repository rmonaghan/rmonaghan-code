import numpy as np
import pandas as pd
import os
import xarray as xr
import rioxarray as rioxr
import geopandas as gpd
import matplotlib.pyplot as plt
from scipy.ndimage._measurements import _stats
from skimage.segmentation import quickshift
import argparse
from scipy import stats
import numpy
import skimage.measure
import skimage.morphology
import skimage
import pandas
import geopandas
from osgeo import gdal, ogr, gdal_array, gdalconst
from geocube.api.core import make_geocube
from geocube.vector import vectorize
from pyproj import CRS
import scipy
import libpysal

import matplotlib
matplotlib.use('TkAgg',force=True)
##
BD_2017 = rioxr.open_rasterio("F:/RMonaghan_Workspace/Projects_Main/GM/SE_Firemap_BD_LCP/Test_Output_2017_07242023/BD_2017.tif").squeeze('band')
BD_labeled_data = BD_2017.copy()
BD_labeled_data.data = np.where(BD_labeled_data == 32767, 20010101, BD_labeled_data.data)
np.unique(BD_labeled_data)

labeled_2 = skimage.measure.label(BD_labeled_data, connectivity=3, background=0)
labeled_2_flip = np.flip(labeled_2)
labeled_2_flip_da = xr.DataArray(labeled_2_flip, coords={'x':BD_2017.x, 'y':BD_2017.y}, dims={'x':BD_2017.x, 'y':BD_2017.y})
# labeled_2_flip_da.plot()

labeled_2_flip_da = xr.DataArray(labeled_2, coords={'x':BD_2017.x, 'y':BD_2017.y}, dims={'x':BD_2017.x, 'y':BD_2017.y})
# labeled_2_flip_da.plot()

labeled_2_flip_da.data = np.where(labeled_2_flip_da.data == 0, 32767, labeled_2_flip_da.data)
labeled_2_flip_da.rio.write_nodata(32767, inplace=True)
labeled_2_flip_da.data = labeled_2_flip_da.data.astype(int)

print(labeled_2_flip_da)

crs = BD_2017.rio.crs

grid_gdf = vectorize(labeled_2_flip_da).set_crs(crs).rename(columns={None:'ID'})
print(grid_gdf.head(5))
print(len(grid_gdf))
plt.figure()


grid_gdf.to_file("F:/RMonaghan_Workspace/Projects_Main/GM/SE_Firemap_BD_LCP/BD_2017_vectorized_connectivityNone_unflipped.shp", engine='pyogrio')
vector.to_parquet("F:/RMonaghan_Workspace/Projects_Main/GM/SE_Firemap_BD_LCP/BD_2017_vectorized_2.parquet")

# file = gpd.read_parquet("F:/RMonaghan_Workspace/Projects_Main/GM/SE_Firemap_BD_LCP/BD_2017_vectorized_2.parquet")
# file.to_file("F:/RMonaghan_Workspace/Projects_Main/GM/SE_Firemap_BD_LCP/BD_2017_vectorized_3.geojson", crs = BD_2017.rio.crs, driver="GeoJSON")
# file.to_file("F:/RMonaghan_Workspace/Projects_Main/GM/SE_Firemap_BD_LCP/BD_2017_vectorized_3.shp", crs = BD_2017.rio.crs)

##
from scipy.stats import mode
vector = vectorize(BD_labeled_data.astype(int)).rename(columns={None:'BD'}).set_crs(crs)
vector['BD'] = pd.to_datetime(vector['BD'].astype(int), format='%Y%m%d')

w = libpysal.weights.Queen.from_dataframe(vector)
print('asd')
# w.plot(vector, width=0.5, s=0)

components = w.components_labels
combined_polygons = vector.dissolve(components)

merged = BD_2017.copy().to_dataset(name='BD').astype(int)
merged['labeled'] = labeled_2_flip_da.astype(int).transpose('y','x')
grouped = merged.BD.groupby(merged.labeled)
grouped.mean()

def mode(x):
    # dims = x.dims
    # da = xr.DataArray(x)
    vals, counts = np.unique(x, return_counts=True)
    mode = np.argmax(counts)
    # da = xr.DataArray(mode, dims=dims)
    return mode

grouped_mode = grouped.map(mode)
grouped

grouped_mode.plot()
grouped_mode.rio.to_raster('C:/Users/rmonaghan/Documents/ArcGIS/Projects/Test_for_Anything/grouped_mode.tif')

x = pd.read_parquet("F:/RMonaghan_Workspace/Projects_Main/GM/Exports/Full_GM_DS.parquet.gzip")
x.head(5)
len(x)
##

from osgeo import gdal, ogr
import sys
# this allows GDAL to throw Python Exceptions
gdal.UseExceptions()

#
#  get raster datasource
#
src_ds = gdal.Open("F:/RMonaghan_Workspace/Projects_Main/GM/SE_Firemap_BD_LCP/Test_Output_2017_07242023/BD_2017.tif")
srcband = src_ds.GetRasterBand(1)

dst_layername = "POLYGONIZED_STUFF"
drv = ogr.GetDriverByName("ESRI Shapefile")
dst_ds = drv.CreateDataSource( dst_layername + ".shp" )
dst_layer = dst_ds.CreateLayer(dst_layername, srs = None )

x = gdal.Polygonize( srcband, None, dst_layer, -1, [], callback=None )
x
## grid_gdf = grid_gdf.rename(columns={'None':'ID'}, inplace=True)
vector.to_file("F:/RMonaghan_Workspace/Projects_Main/GM/SE_Firemap_BD_LCP/BD_2017_vectorized.shp")

def readRaster(fn):
    if not os.path.exists(fn):
        # print "Could not find", fn
        return ([])
    else:
        ds = gdal.Open(fn)
        geotrans = ds.GetGeoTransform()     # georeferences
        prj = ds.GetProjection()            # gets pcrs

        band = ds.GetRasterBand(1)
        nodata = band.GetNoDataValue()
        # print fn, nodata

        data = band.ReadAsArray().astype("float32")
        if nodata != None:
            data[data == nodata] = numpy.nan

        # list of data (numpy array), geotransformation, and projection information
        return ([data, geotrans, prj])

path = "F:/RMonaghan_Workspace/Projects_Main/GM/SE_Firemap_BD_LCP/Test_Output_2017_07242023/BD_2017.tif"











BD_2017 = rioxr.open_rasterio("F:/RMonaghan_Workspace/Projects_Main/GM/SE_Firemap_BD_LCP/Test_Output_2017_07242023/BD_2017.tif").squeeze('band')
BD_labeled_data = BD_2017.copy()
BD_labeled_data.data = np.where(BD_labeled_data == 32767, 0, BD_labeled_data.data)
print(BD_2017)

labeled_1 = skimage.measure.label(BD_labeled_data, connectivity=1, background=0)
labeled_1 = np.flip(labeled_1, axis=0)
labeled_1 = xr.DataArray(labeled_1, coords={'x':BD_2017.x, 'y':BD_2017.y}, dims={'x':BD_2017.x, 'y':BD_2017.y})
plt.figure()
labeled_1.plot()

labeled_2 = skimage.measure.label(BD_labeled_data, connectivity=2, background=0)
labeled_2 = np.flip(labeled_2, axis=0)
labeled_2 = xr.DataArray(labeled_2, coords={'x':BD_2017.x, 'y':BD_2017.y}, dims={'x':BD_2017.x, 'y':BD_2017.y})
plt.figure()
labeled_2.plot()

labeled_3 = skimage.measure.label(BD_labeled_data, connectivity=None, background=0)
labeled_3 = np.flip(labeled_3, axis=0)
labeled_3 = xr.DataArray(labeled_3, coords={'x':BD_2017.x, 'y':BD_2017.y}, dims={'x':BD_2017.x, 'y':BD_2017.y})
labeled_3.where(labeled_3 > 0).plot(cmap='tab10')

v2 = vectorize(BD_2017.astype(int)).reset_index().rename(columns={None:'ID'}).set_crs(crs)
print(v2.head(2))
v2.to_file("C:/Users/rmonaghan/Documents/ArcGIS/Projects/Test_for_Anything/v4.shp", engine='pyogrio')
fig, ax = plt.subplots(2)
ax[0] =  labeled_1.plot(add_colorbar=False)
ax[1] = labeled_2.plot(add_colorbar=False)
plt.show()

src_ds = gdal.Open("F:/RMonaghan_Workspace/Projects_Main/GM/SE_Firemap_BD_LCP/Test_Output_2017_07242023/BD_2017.tif")
srcband = src_ds.GetRasterBand(1)

mask = srcband.where(srcband == 32767)

dst_layername = "POLYGONIZED_STUFF"
drv = ogr.GetDriverByName("ESRI Shapefile")
dst_ds = drv.CreateDataSource(os.getcwd() + '/' + dst_layername + ".shp" )
dst_layer = dst_ds.CreateLayer(dst_layername, srs = None )

gdal.Polygonize( srcband, None, dst_layer, -1, [], callback=None )

# labeled = labeled.transpose('y','x')
# labeled.rio.write_crs(BD_2017.rio.crs, inplace=True)
# labeled.rio.crs
# labeled.plot()
#
# BD_2017 = BD_2017.assign_coords({'band': 'BD'}).expand_dims('band').transpose('y','x','band')
# labeled = labeled.assign_coords ({'band':'labels'}).expand_dims('band').transpose('y','x','band')
# merged = xr.combine_nested([labeled.to_dataset(name='labeled'), BD_2017.to_dataset(name='BD')], concat_dim='band')
# merged
# labeled.transpose('y','x').rio.to_raster("F:/RMonaghan_Workspace/Projects_Main/GM/SE_Firemap_BD_LCP/Test_Output_2017_07242023/BD_2017_Labeled7.tif")
#
# BD_2017
##### READS IN RASTER #####
def readRaster(fn):
    if not os.path.exists(fn):
        # print "Could not find", fn
        return ([])
    else:
        ds = gdal.Open(fn)
        geotrans = ds.GetGeoTransform()     # georeferences
        prj = ds.GetProjection()            # gets pcrs

        band = ds.GetRasterBand(1)
        nodata = band.GetNoDataValue()
        # print fn, nodata

        data = band.ReadAsArray().astype("float32")
        if nodata != None:
            data[data == nodata] = numpy.nan

        # list of data (numpy array), geotransformation, and projection information
        return ([data, geotrans, prj])



##### FOR WRITING OUTPUT: SETS NODATA VAL AND SAVES RASTER #####
def writeResults(outputData, outputFilename, geotrans, prj, nodata=-1, outputRAT=None, imageType=gdal.GDT_Int16):
    out_band = None
    out_dataset = None
    # create the TIF driver for output data
    print("writing data to the raster....", outputFilename)
    driver = gdal.GetDriverByName("GTiff")

    # create the output dataset
    out_dataset = driver.Create(outputFilename, outputData.shape[1], outputData.shape[0], 1, imageType,
                                ['COMPRESS=DEFLATE'])
    out_dataset.SetGeoTransform(geotrans)
    out_dataset.SetProjection(prj)

    # get the output band
    out_band = out_dataset.GetRasterBand(1)

    # determine the nodata value
    if False:
        if imageType == gdal.GDT_Byte:          # if byte nodata == 255
            nodata = 255
        elif imageType == gdal.GDT_UInt16:      # if UInt nodata == 65535
            nodata = (2 ** 16) - 1
        elif imageType == gdal.GDT_Int16:       # if int nodata == -65535
            nodata = -1 * ((2 ** 16) - 1)
        else:                                   # everything else is -9999
            nodata = -9999

    # print("no data value: ", nodata )
    out_band.SetNoDataValue(nodata)
    out_band.WriteArray(outputData)

    if not outputRAT is None:
        print("Saving rat...")
        # print(outputRAT.DumpReadable())
        out_band.SetDefaultRAT(outputRAT)

    # histogram = out_band.GetDefaultHistogram()
    # out_band.SetDefaultHistogram(histogram[0], histogram[1], histogram[3])
    # out_band.FlushCache()

    # out_dataset.BuildOverviews(overviewlist=[3,9,27,81,243,729])
    # out_dataset.FlushCache()

    out_band = None
    out_dataset = None


# raster_dir, annual_dir,
def tabulate_landcover(raster_dir, annual_dir, year, template_fn):
    # confused here--code later reaches into args for year, but nothing in parameters above
    # set which NLCD version to use
    if year < 2001:
        nlcd_fn = raster_dir + "/NLCD_1992.tif"
    elif year < 2004:
        nlcd_fn = raster_dir + "/NLCD_2001.tif"
    elif year < 2006:
        nlcd_fn = raster_dir + "/NLCD_2004.tif"
    elif year < 2008:
        nlcd_fn = raster_dir + "/NLCD_2006.tif"
    elif year < 2011:
        nlcd_fn = raster_dir + "/NLCD_2008.tif"
    elif year < 2013:
        nlcd_fn = raster_dir + "/NLCD_2011.tif"
    elif year < 2016:
        nlcd_fn = raster_dir + "/NLCD_2013.tif"
    else:
        nlcd_fn = raster_dir + "/NLCD_2016.tif"

    # print (year, "NLCD: ", nlcd_fn, os.path.exists(nlcd_fn))
    nlcd_raster = readRaster(nlcd_fn)

    # the ecoregions
    eco_fn = raster_dir + '/ecoregions.tif'     # what is this doing? looks like taking path for ecoregions (somewhere) and generating a filepath?

    ########
    # get the BA data
    ########
    template_fn = annual_dir + '/' + template_fn            # file name template to generate complete filepaths
    bc_fn = template_fn.replace("PRODUCT", "BC")
    bd_fn = template_fn.replace("PRODUCT", "BD")
    bf_fn = template_fn.replace("PRODUCT", "BF")
    bp_fn = template_fn.replace("PRODUCT", "BP")

    '''
    print(eco_fn, os.path.exists(eco_fn))
    print(bc_fn, os.path.exists(bc_fn))
    print(bd_fn, os.path.exists(bd_fn))
    print(bf_fn, os.path.exists(bf_fn))
    print(bp_fn, os.path.exists(bp_fn))
    '''

    eco_raster = readRaster(eco_fn)                     # open up all the rasters
    bc_raster = readRaster(bc_fn)
    bd_raster = readRaster(bd_fn)
    bf_raster = readRaster(bf_fn)
    bp_raster = readRaster(bp_fn)

    ########
    # generate labeled regions for the bf raster
    ########
    bf_raster[0][numpy.isnan(bf_raster[0])] = 0         # nans to 0--for background labeling?
    bf_raster[0][bf_raster[0] >= 1] = 1                 # everything greater than or equal to 1 is 1???
    bf_raster[0] = bf_raster[0].astype(int)             # make int dtype (I guess this makes sense for the BC or BF (filtered BC) product

    # print(year, "BF count", numpy.sum(bf_raster[0]))
    if numpy.sum(bf_raster[0]) == 0:                    # if the BF raster is all 0s, return -1 for error
        return (-1)

    # fill small holes
    # bf_raster[0] = skimage.morphology.remove_small_holes(bf_raster[0], area_threshold=6, connectivity=1)



    bf_labels = skimage.measure.label(bf_raster[0], connectivity=2, background=0)
    # bf_labels += 1  # add 1, background value should now be zero

    # print(year, "BF label count", numpy.unique(bf_labels))
    if numpy.unique(bf_labels).shape[0] == 1:          # if you have one giant lump of the same value, return error
        return (-1)

    bf_labels_fn = template_fn.replace("PRODUCT", "BF_labeled")     # get template ready for output, and output
    writeResults(bf_labels, bf_labels_fn, bf_raster[1], bf_raster[2], nodata=0, outputRAT=None,
                 imageType=gdal.GDT_Int16)

    ########
    # stack rasters into pandas dataframe
    ########
    raster_stack = numpy.stack((eco_raster[0], bf_labels, bp_raster[0], bc_raster[0], bd_raster[0], nlcd_raster[0]),
                               axis=2)
    raster_names = ["ecol3", "bf_label", "bp", "bc", "bd", "nlcd"]

    # reshape the stack
    raster_stack2 = raster_stack.reshape(raster_stack.shape[0] * raster_stack.shape[1], raster_stack.shape[2])
    raster_stack2 = raster_stack2[raster_stack2[:, 0] != 0, :]

    ########
    # convert to pandas dataframe
    ########
    index = range(0, raster_stack2.shape[0])
    combined_table = pandas.DataFrame(raster_stack2, index, raster_names)
    # print("combi table:", combined_table.shape)
    combined_table = combined_table.loc[combined_table['ecol3'] > 0, :]
    # print("combi table after subset:", combined_table.shape)
    combined_table['count'] = 1

    ########
    # generate summaries for labeled images
    ########
    combined_summary_part1 = combined_table.groupby('bf_label', as_index=False).agg({ \
        'count': 'sum', \
        # 'ecol3':lambda x:stats.mode(x)[0], \
        'ecol3': lambda x: pandas.Series.mode(x)[0], \
        'bp': ['min', 'max', 'mean', 'std'], \
        'bc': ['min', 'max', 'mean', 'std'], \
        'bd': ['min', 'max', 'mean', 'std']
    })
    combined_summary_part1.reset_index(inplace=True)

    combined_summary_part2 = combined_table.groupby(['bf_label', 'nlcd'], as_index=False).agg({'count': 'sum'})

    # reshape the nlcd summaries
    combined_summary_part22 = combined_summary_part2.pivot(index='bf_label', columns='nlcd', values='count')
    combined_summary_part22.reset_index(inplace=True)

    # join the BA summaries
    combined_summary = combined_summary_part1.merge(right=combined_summary_part22, on="bf_label", how="outer")

    ########
    # fix the column names
    ########
    colnames_dict = {}

    for c in combined_summary.columns:
        if isinstance(c, tuple):
            c_new = str(c[0])
            if len(c[1]) > 0:
                if c[1] != "<lambda>":
                    c_new += "_" + str(c[1])
        elif isinstance(c, str):
            c_new = c
        else:
            c_new = "nlcd_" + str(int(c))
        colnames_dict.update({c: c_new})

    combined_summary = combined_summary.rename(index=str, columns=colnames_dict)

    ########
    # make sure every possible NLCD value has a column
    ########
    all_NLCD_values = [11, 12, 21, 22, 23, 24, 31, 32, 33, 41, 42, 43, 51, 52, 61, 71, 81, 82, 83, 84, 85, 90, 91, 92,
                       95]
    all_NLCD_columns = ["nlcd_" + str(i) for i in all_NLCD_values]

    all_other_columns = []
    for s in combined_summary.columns:
        if not s in all_NLCD_columns:
            if not s in all_other_columns:
                all_other_columns += [s]

    combined_summary2 = combined_summary.loc[:, all_other_columns]
    for c in all_NLCD_columns:
        if c in combined_summary.columns:
            combined_summary2[c] = combined_summary[c]
        else:
            combined_summary2[c] = numpy.nan

    # replace NA with zeros
    combined_summary2.fillna(value=0, inplace=True)

    # standardize the NLCD columns
    combined_summary2["nlcdr_11"] = combined_summary2["nlcd_11"]  # water
    combined_summary2["nlcdr_12"] = combined_summary2["nlcd_12"]  # snow/ice
    combined_summary2["nlcdr_20"] = combined_summary2["nlcd_21"] + combined_summary2["nlcd_22"] + combined_summary2[
        "nlcd_23"] + combined_summary2["nlcd_24"] + combined_summary2["nlcd_85"]  # developed
    combined_summary2["nlcdr_31"] = combined_summary2["nlcd_31"] + combined_summary2["nlcd_32"] + combined_summary2[
        "nlcd_33"]  # barren
    combined_summary2["nlcdr_40"] = combined_summary2["nlcd_41"] + combined_summary2["nlcd_42"] + combined_summary2[
        "nlcd_43"]  # forest
    combined_summary2["nlcdr_52"] = combined_summary2["nlcd_51"] + combined_summary2["nlcd_52"]  # shrub
    combined_summary2["nlcdr_71"] = combined_summary2["nlcd_71"]  # grassland
    combined_summary2["nlcdr_81"] = combined_summary2["nlcd_81"] + combined_summary2["nlcd_84"]  # pasture/hay
    combined_summary2["nlcdr_82"] = combined_summary2["nlcd_82"] + combined_summary2["nlcd_83"]  # cultivated crops
    combined_summary2["nlcdr_90"] = combined_summary2["nlcd_90"] + combined_summary2["nlcd_91"]  # forested wetlands
    combined_summary2["nlcdr_95"] = combined_summary2["nlcd_92"] + combined_summary2[
        "nlcd_95"]  # emergent herbaceous wetlands

    # which is the dominant NLCD class?
    simplified_nlcd_cols = ["nlcdr_11", "nlcdr_12", "nlcdr_20", "nlcdr_31", "nlcdr_40", "nlcdr_52", "nlcdr_71",
                            "nlcdr_81", "nlcdr_82", "nlcdr_90", "nlcdr_95"]
    dominant_class = combined_summary2.loc[:, simplified_nlcd_cols].idxmax(axis=1)
    combined_summary2["nlcdr_dominant"] = dominant_class

    # convert labels to polygons, delete existing shapefile first
    bf_labels_shapefile = bf_labels_fn.replace(".tif", ".shp")

    if os.path.exists(bf_labels_shapefile):
        driver = ogr.GetDriverByName("ESRI Shapefile")
        print("Deleting " + bf_labels_shapefile)
        driver.DeleteDataSource(bf_labels_shapefile)

    # cmd = 'gdal_polygonize.py -q -8 ' + bf_labels_fn + ' ' + bf_labels_shapefile + ' -f "ESRI Shapefile"'
    # os.system(cmd)
    bf_label_ds = gdal.Open(bf_labels_fn)
    bf_label_band = bf_label_ds.GetRasterBand(1)
    srs = osr.SpatialReference(bf_label_ds.GetProjection())

    bf_ds = gdal.Open(bf_fn)
    bf_band = bf_ds.GetRasterBand(1)

    driver = ogr.GetDriverByName("ESRI Shapefile")
    bf_labels_poly = driver.CreateDataSource(bf_labels_shapefile)
    bf_labels_poly_layer = bf_labels_poly.CreateLayer(os.path.basename(bf_labels_shapefile).replace(".shp", ""),
                                                      srs=srs)
    newField = ogr.FieldDefn('DN', ogr.OFTInteger)
    bf_labels_poly_layer.CreateField(newField)
    gdal.Polygonize(bf_label_band, bf_band, bf_labels_poly_layer, 0, [], callback=None)
    bf_labels_poly.Destroy()
    bf_labels_poly = None

    # join the attributes to the polygons
    bf_labels_shapes = geopandas.read_file(bf_labels_shapefile)
    combined_summary2 = combined_summary2.loc[:, ~combined_summary2.columns.duplicated()]

    # combined_summary2.set_index(keys="bf_label", inplace=True)
    bf_labels_shapes2 = bf_labels_shapes.merge(right=combined_summary2, left_on="DN", right_on="bf_label", how="outer")
    bf_labels_shapes2 = bf_labels_shapes2.loc[bf_labels_shapes2["bf_label"] != 0, :]

    # remove extra id columns
    bf_labels_shapes2.drop(labels=['index', 'DN'], axis=1, inplace=True)

    bf_labels_shapes2.to_file(bf_labels_shapefile, driver="ESRI Shapefile")

    # print(year, tile_dir, combined_summary2.columns)


def tabulate_landcover_mapper(args):
    tabulate_landcover(
        raster_dir=args[0],
        annual_dir=args[1],
        year=args[2],
        template_fn=args[3]
    )


# if __name__ == '__main__':
def main():
    total_start_time = time.time()

    rank = 0

    # get args from command line
    parser = argparse.ArgumentParser()
    parser.add_argument('-r', '--region', required=True, help='Region to process. (Required)', dest='region')
    parser.add_argument('-s', '--stack_file', help="Stack file to read. (Required).", required=True, dest='stack_file',
                        default=None)
    parser.add_argument('--min_year', required=False, help='Minimum year to generate samples for (2021)',
                        default='2021', dest='min_year')
    parser.add_argument('--max_year', required=False, help='Maximum year to generate samples for (2022)',
                        default='2022', dest='max_year')
    parser.add_argument("-rd", "--raster_dir", help="Directory containing rasters to summarize (e.g. NLCD; Required)",
                        required=True, dest="raster_dir")
    parser.add_argument("-ad", "--annual_dir", help="Directory containing annual products in (Required)", required=True,
                        dest="annual_dir")
    parser.add_argument('-d', '--prod_date', help="Production date string (Required)", required=True, dest='prod_date',
                        default='20210331')
    parser.add_argument('-n', '--n_jobs', help="Number of multiprocessing Pool workers to use (1)", required=False,
                        dest='n_jobs', default='1')
    parser.add_argument('-p', '--parallel', default='multiprocessing',
                        help='Parallel processing mode (None|multiprocessing|mpi)', dest='parallel')
    args = parser.parse_args()

    # check to make sure the stack file exists
    stack_file = args.stack_file
    if stack_file == None:
        print('Stack file was not defined!')
        exit()

    if not os.path.exists(stack_file):
        print('Stack file (' + stack_file + ') does not exist!')
        exit()

    min_year = int(args.min_year)
    max_year = int(args.max_year)

    raster_dir = args.raster_dir
    if not os.path.exists(raster_dir):
        print('Raster directory (' + raster_dir + ') does not exist!')
        exit()

    annual_dir = args.annual_dir
    if not os.path.exists(annual_dir):
        if rank == 0:
            print('Annual products directory (' + annual_dir + ') does not exist!  Creating...')
            os.mkdir(annual_dir)

    # get the production date string
    prod_date = args.prod_date

    # region
    region = args.region

    # number of jobs
    n_jobs = int(args.n_jobs)
    parallel = args.parallel

    if parallel == 'multiprocessing':
        print('Importing multiprocessing')
        from multiprocessing import Pool

    elif parallel == 'mpi':
        if rank == 0:
            print('Importing MPI')
        from mpi4py import MPI
        status = MPI.Status()
        comm = MPI.COMM_WORLD
        # n_ranks = comm.Get_size()
        n_ranks = n_jobs
        rank = comm.Get_rank()

    if rank == 0:
        print("################################################################################")
        print("################################################################################")
        print("#", os.path.basename(__file__))
        print(time.strftime("# Processing started at %d/%m/%Y %H:%M"))
        print("#")
        print("# region:", region)
        print("# stack_file:", stack_file)
        print("# raster_dir:", raster_dir)
        print("# annual_dir:", annual_dir)
        print("# prod_date:", prod_date)
        print('# n_jobs:', n_jobs)
        print("# parallel:", parallel)
        print("#")
        print("################################################################################")
        print("################################################################################")

    # open the stack
    stack = pandas.read_csv(stack_file)
    stack = stack.loc[stack['OUTPUT'] == 1, :]
    stack.reset_index(inplace=True)
    # print(stack)

    tile_h = stack.loc[0, 'HH']
    tile_v = stack.loc[0, 'VV']
    tile_num = "%0.3d%0.3d" % (tile_h, tile_v)
    tile_long = ('%0.3d%0.3f' % (tile_h, tile_v))

    # stack.sort_values('YEAR', inplace=True)
    # stack.reset_index(inplace=True)

    # years = numpy.sort(numpy.unique(stack['YEAR']))
    years = numpy.arange(min_year, max_year + 1, 1)

    arg_list = []

    for year in years:
        template_fn = "LBA_CU_" + tile_num + "_" + str(year) + "_" + prod_date + "_C02_PRODUCT.tif"
        arg_list += [[raster_dir, annual_dir, year, template_fn]]

    # process the images and predictors
    if parallel == 'multiprocessing':
        pool = Pool(processes=n_jobs)
        pool.map(tabulate_landcover_mapper, arg_list)
        pool.close()

    elif parallel == 'mpi':
        rank_id = 0  # rank to assign jobs to
        for i in range(0, len(arg_list)):
            if rank_id == rank:
                print('rank', rank, 'processing', i, 'of', len(arg_list))
                tabulate_landcover_mapper(arg_list[i])

            rank_id += 1

            if rank_id == n_ranks:
                rank_id = 0

        comm.Barrier()
    else:
        for i in range(0, len(arg_list)):
            print('rank', rank, 'processing', i, 'of', len(arg_list))
            tabulate_landcover_mapper(arg_list[i])

    if rank == 0:
        print("########################################################################################")
        print("# Finished vectorizing annual BF data for", time.time() - total_start_time, "(s)")
        print("########################################################################################")

    return (True)

import geopandas as gpd
import pandas as pd
import numpy as np

se = gpd.read_file('R:/Projects/NRCS_BurnPermit/SE_BurnGDB/SE_BurnData2010_2022.gdb', layer='ALLStates_BurnData')
se
print('hey')
ORDER OF OPERATIONS:

*Slightly more detailed notes of main findings available in /GM/Notes/ <-- these largely correspond to presentations in /GM/Presentations/

	1. Downloaded netcdf files, convert to tifs - /Data/GRIDMET_files/ (both netcdf + tif versions available here, as well as wget dl script used for initial download)
	2. Sampled netcdf files (points from FPA FOD and Burn Permit DB ***Only for FL***) -- /GM_Scripts/Preprocessing/GRIDMET_tif_Sampling.ipynb
		- Sampled CSVs available in: /Exports/
			- This data includes FFS Districts, which were dropped later on 
		- GM concatenated point data (unjoined to any dataset) available in parquet form under filepath: /Exports/Full_GM_DS.parquet.gzip (open with pd.read_parquet())
	3. /GM_Scripts/Preprocessing/GM_Regional_Chunks.ipynb
		- Weekly analysis (have since nixed any kind of resampling with time interval)
		- Added FL regions (arbitrary divisions, but mimics ecoregion level III divisions)
		- Began analyzing year 2017 
	4. Visualizations to detect relationships/trends/anything of import -- /GM_Scripts/EDA_and_Viz/
		- GM_Heatmaps_and_Distributions.ipynb: 
			- Covariance and correlation heatmaps
			- (Monthly) Standard deviation kde distplots for BP and WF, with mean vertical line
			- (Quarterly) Standard deviation kde plots for BP and WF per each FFS District
			- (Quarterly) Standard deviation kde plots for BP and WF, with mean vertical line
		- GM_Wildfire_Class_EDA.ipynb - EDA/Viz with NWCG Cause Classificaiton + NWCG General Case:
			- Kde density plots for FPA FOD + GM variables separated by cause classification (human, missing/undetermined, natural)
				- Mainly looked into because of high values for precipitation in WFs (lightning-caused WFs)
		- GM_DistributionPlots_and_PCA.ipynb:
			- Weekly data, with arbitrary area classes ('Size Class')
			- Plots
				- Plots showing WF/BP counts by week, WF/BP area by week (2017)
				- Kde density plots showing std dev variation of WF/BP per week categorized by size class and region
				- More std dev plots, categorized only by fire type (Wf or BP)
			- PCA - ran for both WF and BP using gridMET variables
				- Visualized number of components necessary to reach 90% of explained variance (cumulative) - for both fire types, this was 5 components
				- Dataframe with components and signs (more explanation of this in /GM/Notes/Notes_1.docx) - not visualized, but code is there
	5. /GM_Scipts/Analysis 
		- RF_funcs.ipynb <-- this is currently the only script which does any major analysis. It includes a pipeline that creates plots (for each year) investigating
		  feature selection to reduce multicollinearity/correlation, forward selection, random forest, random forest metrics (including permutation imporance). Plots/tables include:
			- Correlation heatmaps - available after run of each step in pipeline
			- Hierarchal clustering dendrograms - available after run of each step in pipeline
			- Variance inflation factor - available after run of each step in pipeline
			- Table/dataframe for all pipeline runs (not the nicest-looking output, but can be prettified with some formatting)
			- Output available at /Exports/Yearly_RF.csv
			* Note: Permutation importance plots are shown in Presentation folder--the code for this is not included in the script, but calculation of permutation importance
			  is
	6. Connect this analysis to SE Firemap --> /GM/SE_Firemap_BD_LCP/
		- Main objectives:
			- /GM/SE_FIREMAP_BD_LCP/Scene_DT_cleaned.ipynb
			- 	Get annual burn date
			- 	Get last clear pixel for burn date
			- Label patches, use majority filter to get majority date for patch
			- /GM/SE_FIREMAP_BD_LCP/Annual_Clustering_2017.py
				- Intersect/pull from gridMET to assign each day from LCP to BD as a "WF day" or an "Rx day" --> give majority days/week to the patch classification
					- The last bit ("give majority...") may be rule-based, or RF-based (to me, RF was the easiest, because you can assign a probability to each classification with sklearn,
					  and, you're letting the data speak for itself)


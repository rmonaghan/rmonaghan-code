ORDER OF OPERATIONS
1. 	AirSensor_dl.ipynb: Download AirNow data, which is found in nested tree structure on website. Site is dynamically generated, so libraries like 
	BeautifulSoup will not work. The code in this script is mainly just to grab a hold of file URLs from AWS from page source/html, and download them.
2.	Hourly_Data_Manipulation.ipynb: Concatenate downloads from above script. Manipulate data to local TZ, subset exceedances.
3.	Combining_GM_Metar.ipynb: Combines METARs downloaded from Iowa Environmental Mesonet. Makes spatial. Combines with gridMET 100-Hour and 1000-Hour
	Fuel Moisture. (Note: Attempted to use python package metar for parsing, but this package seems to be useful for individual reports, and not bulk.
4. 	GOES_ADPF.ipynb: Process GOES ADP data, reformat data into geographic CRS, subset spatially and temporally, and make animation. This product has
	some big drawbacks, especially with respect to this project: it is a qualitative product based on surface reflectance, and therefore only a 
	daytime product. Exceedances in air sensors were best detected overnight. Therefore, animations can only be helpful to see where movement for both aerosol and smoke were
	during the day. (Note: playing with the data's UTC time format was not simple. I would recommend, if using, to stay away from an actual
	conversion, but instead creating a new [converted] field or converting on-the-fly. 